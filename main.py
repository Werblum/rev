from joblib import load
import numpy as np
import pandas as pd
from fastapi import FastAPI, Body
from starlette.responses import FileResponse, JSONResponse
from geopy.geocoders import Nominatim

app = FastAPI()
geolocator = Nominatim(user_agent="Get_geo")


@app.post("/predict")
def predict(data=Body()):
    test_data = [
        data["region"],
        data["total_area"],
        data["floor"],
        data["floors_number"],
        data["house_material"],
        get_address_coordinates(data["address"])[0],
        get_address_coordinates(data["address"])[1],
        data["proximity_metro"],
        get_age_house(int(data["age_house"]))
    ]

    with open("model/model_rf.joblib", "rb"):
        model = load('model/model_rf.joblib')
    with open("model/encoder.joblib", "rb"):
        encoder = load('model/encoder.joblib')

    row = np.array(test_data)

    columns = ['region', 'total_area', 'floor', 'floors_number', 'house_material', 'longitude', 'latitude',
               'proximity_metro', 'age_house']

    X = pd.DataFrame([row], columns=columns)

    X_cat = X[['region', 'house_material', 'proximity_metro']]

    X_num = X[['total_area', 'floor', 'floors_number', 'longitude', 'latitude', 'age_house']]

    X_encoded = pd.DataFrame(encoder.transform(X_cat),
                             columns=encoder.get_feature_names_out(X_cat.columns))

    X = pd.concat([X_encoded.reset_index(drop=True),
                   X_num.reset_index(drop=True)], axis=1)

    return JSONResponse(content={"message": fr"{round(model.predict(X)[0]):,} ₽".replace(",", " "),
                                 "message2": f"{round(model.predict(X)[0] / float(data['total_area'])):,} ₽"
                        .replace(",", " "),
                                 "message3": fr"{round(model.predict(X)[0] - (model.predict(X)[0] * 0.07433)):,} - "
                        .replace(",", " ") + f"{round(model.predict(X)[0] + (model.predict(X)[0] * 0.07433)):,} ₽"
                        .replace(",", " ")})


@app.get("/")
def root():
    return FileResponse("templates/index.html")


def get_address_coordinates(address: str):
    address = address.replace(",", " ")
    location = geolocator.geocode(address)
    return float(location.longitude), float(location.latitude)


def get_age_house(data: int):
    if data >= 2024:
        return 0
    elif data < 2024:
        return 2024 - data
